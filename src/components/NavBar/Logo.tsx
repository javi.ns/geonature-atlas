import { Stack, makeStyles, Typography } from '@material-ui/core'
import { useRouter } from 'next/router'
import { FC } from 'react'

const useStyles = makeStyles({
  root: {
    cursor: 'pointer',
  },
  title: {
    padding: 0,
    margin: 0,
    fontWeight: 700,
    textTransform: 'uppercase',
  },
  subtitle: {
    padding: 0,
    margin: 0,
  },
})

const Logo: FC = () => {
  const classes = useStyles()
  const router = useRouter()

  const handleGoToHome = () => {
    router.push('/')
  }

  return (
    <Stack
      className={classes.root}
      direction="row"
      justifyContent="flex-start"
      alignItems="center"
      spacing={1}
      onClick={handleGoToHome}
    >
      <Typography className={classes.title} color="primary">
        Geonature
      </Typography>
      <Typography className={classes.subtitle} color="primary">
        Zones Humides
      </Typography>
    </Stack>
  )
}

export default Logo
